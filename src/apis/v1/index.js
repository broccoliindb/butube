const apis = require('express').Router()
const { getUpdatedUserInfo, updateUserInfo } = require('./User')
const { uploadVideo } = require('./Video')
apis.get('/api/v1/update-user-info', getUpdatedUserInfo)
apis.post('/api/v1/update-user-info', updateUserInfo)
apis.post('/api/v1/upload-video', uploadVideo)

module.exports = apis
