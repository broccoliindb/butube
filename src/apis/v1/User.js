const User = require('../../models/User')

const updateUserInfo = async (req, res) => {
  const { user: { _id } } = req
  const {
    body: {
      lastName,
      firstName,
      email,
      password,
      tel,
      gender,
      birth,
      nickName
    }
  } = req
  console.log('body', req.body)
  let user = {}
  try {
    await User.findOneAndUpdate({ _id: _id }, {
      $set: {
        lastName: lastName ? lastName : req.user.lastName,
        firstName: firstName ? firstName: req.user.firstName,
        email: email ? email : req.user.email,
        password: password ? password : req.user.password,
        tel: tel ? tel : req.user.tel,
        gender: gender ? gender : req.user.gender,
        birth: birth ? birth : req.user.birth,
        nickName: nickName ? nickName : req.user.nickName,
      }
    })
    user = await User.findById({ _id })
    console.log('after', user)
  } catch (err) {
    console.error(err)
    return res.status(500).send(err)
  }
  return res.status(200).send(user)
}

const getUpdatedUserInfo = async (req, res) => {
  const { user: { _id } } = req
  let user = {}
  try {
    user = await User.findById({ _id: _id })
  } catch (err) {
    console.error(err)
    return res.status(500).send(err)
  }
  return res.status(200).send(user)
}

module.exports = {
  getUpdatedUserInfo,
  updateUserInfo
}
