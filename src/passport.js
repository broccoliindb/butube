const passport = require('passport')
const LocalStrategy = require('passport-local').Strategy
const User = require('./models/User')

const processLogin = (email, password, done) => {
  User.findOne({ email }, (err, user) => {
    if (err) {
      return done(err)
    }
    if (!user) {
      return done(null, false, { message: 'incorrect user' })
    }
    if (user.password !== password) {
      return done(null, false, { message: 'Incorrect password.' })
    }
    return done(null, user)
  })
}

passport.use(new LocalStrategy({
  usernameField: 'email',
  passwordField: 'password'
}, processLogin))

passport.serializeUser((user, done) => {
  done(null, user._id)
})

passport.deserializeUser((id, done) => {
  User.findById(id, (err, user) => {
    done(err, user)
  })
})

module.exports = passport
