const express = require('express')
const globalRouter = express.Router()
const { home } = require('../controllers/globalController')

globalRouter.get('/', home)

module.exports = globalRouter
