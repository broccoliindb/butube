const express = require('express')
const { getUpload, postUpload } = require('../controllers/videoController')
const { videoUpload } = require('../middleware')

const videoRouter = express.Router()

videoRouter.get('/upload', getUpload)
videoRouter.post('/upload', videoUpload.single('videoFile'), postUpload)

module.exports = videoRouter
