const { getLogin, getLogout, postLogin, getJoin, postJoin, getProfile, postProfile, postAvatar } = require('../controllers/userController')
const passport = require('../passport')
const userRouter = require('express').Router()
const { avatarUpload } = require('../middleware')

userRouter.get('/login', getLogin)
userRouter.get('/logout', getLogout)
userRouter.post('/login', passport.authenticate('local',
  {
    successRedirect: '/',
    failureRedirect: '/login',
    failureFlash: 'Invalid username or password',
    successFlash: 'Welcome!'
  }
), postLogin)
userRouter.get('/join', getJoin)
userRouter.post('/join', postJoin)
userRouter.get('/profile', getProfile)
userRouter.post('/profile', postProfile)
userRouter.post('/avatar', avatarUpload.single('avatar'), postAvatar)

module.exports = userRouter
