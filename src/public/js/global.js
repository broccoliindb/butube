const main = document.querySelector('main')
const avatarUploadOverLay = document.querySelector('.avatarUploadOverLay')

const getUniqueId = () => {
  return Math.random().toString(32).substring(2)
}

const handlePopover = (evt) => {
  const target = evt.target
  if (target.closest('.avatar__edit') && avatarUploadOverLay) {
    avatarUploadOverLay.classList.toggle('selected')
  } else {
    if (target.closest('.avatarUpload')) return
    if (avatarPopover) {
      avatarPopover.classList.remove('show')
    }
    if (avatarUploadOverLay) {
      const tempImg = avatarUploadOverLay.querySelector('.tempAvatar')
      if (tempImg) {
        tempImg.remove()
      }
      avatarContainer.classList.remove('previewAvatar')
      avatarUploadOverLay.classList.remove('selected')
    }
  }
}

const handleAvatarUploadOverlay = (evt) => {
  const target = evt.target
  if (!target.closest('.avatarUpload')) {
    handleAvatarUploadOverlay.classList.remove('selected')
  }
}

if (avatarUploadOverLay) {
  avatarUploadOverLay.addEventListener('click', handleAvatarUploadOverlay)
} else {
  avatarUploadOverLay.removeEventListener('click', handleAvatarUploadOverlay)
}
main.addEventListener('click', handlePopover)
