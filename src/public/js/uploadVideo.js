const uploadContainer = document.querySelector('.uploadContainer')
const dragVideoArea = uploadContainer.querySelector('.videoUpload')

let videoToUpload = null

const getUpdatedVideoList = () => {

}

const hideUpload = () => {
  if (!uploadContainer.classList.contains('hide')) {
    uploadContainer.classList.add('hide')
  }
}

const handleUploadVideo = () => {
  const formData = new FormData()
  const textArea = uploadContainer.querySelector('textarea')
  formData.append('videoDescription', textArea.value)
  const genres = []
  const inputs = uploadContainer.querySelectorAll('input')
  inputs.forEach(input => {
    console.log(input)
    if (input.name === 'genres' && input.checked) {
      genres.push(input.id)
    } else if (input.name !== 'genres' && input.name !== 'videoFile') {
      formData.append(input.name, input.value)
    }
  })
  formData.append('genres', genres)
  formData.append('videoFile', videoToUpload)
  fetch('/upload', {
    method: 'post',
    body: formData
  }).then(data => {
    getUpdatedVideoList()
    hideUpload()
    videoToUpload = null
  }).catch(err => {
    console.error(err)
  })
}

const previewVideo = (video) => {
  const temp = dragVideoArea.querySelector('.tempVideo')
  if (temp) {
    temp.remove()
  }
  const reader = new FileReader()
  reader.readAsDataURL(video)
  reader.onloadend = () => {
    const video = document.createElement('video')
    video.className = 'tempVideo'
    video.src = reader.result
    document.querySelector('.preview').appendChild(video)
  }
}

const handleVideo = (videos) => {
  if (videos && videos.length) {
    videoToUpload = videos[0]
    if (!dragVideoArea.classList.contains('previewVideo')) {
      dragVideoArea.classList.add('previewVideo')
    }
    const video = videos[0]
    previewVideo(video)
  }
}

const dragVideoHandler = (evt) => {
  evt.preventDefault()
  evt.stopPropagation()
  if (evt.type === 'dragenter') {

  } else if (evt.type === 'drop') {
    const dt = evt.dataTransfer
    const files = dt.files
    if (files.length > 2) {
      alert('2개이상의 이미지임. 하나만 넣어라!')
      return
    }
    handleVideo(files)
  } else if (evt.type === 'dragleave') {

  } else {

  }
}

const uploadVideoInit = () => {
  if (!uploadContainer) return
  const uploadBtn = uploadContainer.querySelector('.uploadVideoBtn')
  uploadBtn.addEventListener('click', handleUploadVideo)

  if (!dragVideoArea) return
  ['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
    dragVideoArea.addEventListener(eventName, dragVideoHandler)
  })
}

uploadVideoInit()
