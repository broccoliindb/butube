const avatarPopoverBtn = document.querySelector('#jsAvatarPopover')
const avatarPopover = document.querySelector('.avatarPopover')
const avatarEditBtn = document.querySelector('.editAvatar')

const handleShowProfilePopover = () => {
  if (avatarPopover) {
    avatarPopover.classList.toggle('show')
  }
}

const handleEditAvatar = () => {
  console.log('add')
}

const init = () => {
  avatarPopoverBtn.addEventListener('click', handleShowProfilePopover)
}

if (avatarPopover && avatarPopoverBtn) {
  init()
  if (avatarEditBtn) {
    avatarEditBtn.addEventListener('click', handleEditAvatar
    )
  }
}
