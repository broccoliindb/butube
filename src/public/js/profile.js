const profileContainer = document.querySelector('.profileContainer')

const setUpdatedUserInfo = (userInfo) => {
  const name = profileContainer.querySelector('.name')
  const email = profileContainer.querySelector('.email')
  const password = profileContainer.querySelector('.password')
  const tel = profileContainer.querySelector('.tel')
  const gender = profileContainer.querySelector('.gender')
  const birthDisplay = profileContainer.querySelector('.birthDisplay')
  const nickName = profileContainer.querySelector('.nickName')
  if (name) {
    name.innerHTML = `${userInfo.lastName} ${userInfo.firstName}`
  }
  if (email) {
    email.innerHTML = userInfo.email
  }
  if (password) {
    password.innerText = userInfo.password
  }
  if (tel) {
    tel.innerHTML = userInfo.tel
  }
  if (gender) {
    gender.innerHTML = userInfo.gender
  }
  if (birthDisplay) {
    const date = new Date(userInfo.birth)
    birthDisplay.innerHTML = `${date.getFullYear()}년 ${date.getMonth() + 1}월 ${date.getDate()}일`
  }
  if (nickName) {
    nickName.innerHTML = userInfo.nickName
  }
}

const save = async (data) => {
  let result = null
  console.log('data', data, JSON.stringify(data))
  try {
    const response = await fetch('/api/v1/update-user-info', {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json' }
    })
    if (response.ok) {
      result = await response.json()
      setUpdatedUserInfo(result)
      console.log('result', result)
    }
  } catch (err) {
    result = err
  }
  return result
}

const getEditInfo = (info) => {
  const inputs = info.querySelectorAll('input')
  const item = {}
  if (inputs) {
    for (const input of inputs) {
      if (input.name === 'gender') {
        item.gender = 'secret'
        if (input.checked) {
          item.gender = input.id
          break
        }
      } else if (input.name === 'birth') {
        const days = input.value.split('-')
        const birthday = new Date(days[0], days[1] - 1, days[2])
        item.birth = birthday.toISOString()
        console.log('birth', item.birth, typeof item.birth)
      } else {
        item[input.name] = input.value
      }
    }
  }
  save(item)
}

const resetShowClass = (parentItem, selectedArrow) => {
  selectedArrow.innerHTML = '<i class="fas fa-chevron-right"></i>'
  selectedArrow.classList.remove('show')
  parentItem.classList.remove('show')
}

const resetAllShowClass = () => {
  const parentItems = profileContainer.querySelectorAll('.item')
  parentItems.forEach(parentItem => {
    parentItem.classList.remove('show')
    const arrow = parentItem.querySelector('.arrow')
    if (arrow && arrow.classList.contains('show')) {
      arrow.classList.remove('show')
      arrow.innerHTML = '<i class="fas fa-chevron-right"></i>'
    }
  })
}

const handleArrowClicked = (evt) => {
  const target = evt.target
  const selectedArrow = target.closest('.arrow')
  const parentItem = target.closest('.item')
  const edit = target.closest('.edit')
  const update = target.closest('.update')
  const oldParentItem = profileContainer.querySelector('.item.show')
  if (edit) {
    if (update) {
      getEditInfo(edit)
    }
    return
  }
  if (!selectedArrow) {
    resetAllShowClass()
    return
  }
  if (oldParentItem) {
    if (parentItem.id === oldParentItem.id) {
      resetShowClass(parentItem, selectedArrow)
      return
    } else {
      const oldSelectedArrow = oldParentItem.querySelector('.arrow')
      resetShowClass(oldParentItem, oldSelectedArrow)
    }
  }
  selectedArrow.innerHTML = '<i class="fas fa-chevron-down"></i>'
  selectedArrow.classList.add('show')
  parentItem.classList.add('show')
  const input = parentItem.querySelector('input')
  input.focus()
}

if (profileContainer) {
  const parentItems = profileContainer.querySelectorAll('.item')
  parentItems.forEach(item => {
    item.id = getUniqueId()
  })
  profileContainer.addEventListener('click', handleArrowClicked)
}
