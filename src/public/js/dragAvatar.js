const dragAvatarArea = document.querySelector('#jsDropArea')
const avatarContainer = document.querySelector('.avatarUpload')
const uploadAjax = document.querySelector('.uploadAvatarBtn')
const avatarImage = document.querySelector('#jsAvatarPopover')
let photoToUpload = null
const previewAvatar = (photo) => {
  const temp = avatarContainer.querySelector('.tempAvatar')
  if (temp) {
    temp.remove()
  }
  const reader = new FileReader()
  reader.readAsDataURL(photo)
  reader.onloadend = () => {
    const img = document.createElement('img')
    img.className = 'tempAvatar'
    img.src = reader.result
    document.querySelector('.preview').appendChild(img)
  }
}

const handleAvatar = (avatar) => {
  if (avatar && avatar.length) {
    photoToUpload = avatar[0]
    if (!avatarContainer.classList.contains('previewAvatar')) {
      avatarContainer.classList.add('previewAvatar')
    }
    const photo = avatar[0]
    previewAvatar(photo)
  }
}

const dragAvatarHandler = (evt) => {
  evt.preventDefault()
  evt.stopPropagation()
  if (evt.type === 'dragenter') {

  } else if (evt.type === 'drop') {
    const dt = evt.dataTransfer
    const files = dt.files
    if (files.length > 2) {
      alert('2개이상의 이미지임. 하나만 넣어라!')
      return
    }
    handleAvatar(files)
  } else if (evt.type === 'dragleave') {

  } else {

  }
}

const getUpdatedUserInfo = async () => {
  try {
    const response = await fetch('/api/v1/update-user-info')
    if (response.ok) {
      const data = await response.json()
      const profileImg = document.querySelector('.profile')
      avatarImage.src = data.avatar
      if (profileImg) {
        const img = profileImg.querySelector('img')
        img.src = data.avatar
      }
      if (avatarPopover) {
        const profileImg = avatarPopover.querySelector('img')
        profileImg.src = data.avatar
      }
      const tempImg = avatarUploadOverLay.querySelector('.tempAvatar')
      if (tempImg) {
        tempImg.remove()
      }
      avatarContainer.classList.remove('previewAvatar')
      avatarUploadOverLay.classList.remove('selected')
      if (avatarPopover) {
        avatarPopover.classList.remove('show')
      }
    }
  } catch (err) {
    console.error(err)
  }
}

const handleUpload = () => {
  const formData = new FormData()
  formData.append('avatar', photoToUpload)
  fetch('/avatar', {
    method: 'post',
    body: formData
  }).then(data => {
    getUpdatedUserInfo()
    photoToUpload = null
  }).catch(err => {
    console.error(err)
  })
}

const dragAvatarInit = () => {
  if (dragAvatarArea && avatarContainer && uploadAjax) {
    ['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
      dragAvatarArea.addEventListener(eventName, dragAvatarHandler)
    })
    uploadAjax.addEventListener('click', handleUpload)
  } else {
    ['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
      dragAvatarArea.removeEventListener(eventName, dragAvatarHandler)
    })
    uploadAjax.removeEventListener('click', handleUpload)
  }
}

dragAvatarInit()
