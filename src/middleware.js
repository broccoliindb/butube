const multer = require('multer')

const avatarUpload = multer({ dest: 'uploads/avatars/' })
const videoUpload = multer({ dest: 'uploads/videos/' })

const localMiddleware = (req, res, next) => {
  res.locals.user = req.user || {}
  res.locals.isAuthenticated = !!req.user
  res.locals.siteName = 'BUTUBE'
  next()
}

module.exports = {
  localMiddleware,
  avatarUpload,
  videoUpload
}
