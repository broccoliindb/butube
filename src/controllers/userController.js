const User = require('../models/User')

const getLogin = (req, res) => {
  return res.render('login', {
    pageTitle: 'login',
    messages: req.flash('error')
  })
}

const getLogout = (req, res) => {
  req.session.destroy((err) => {
    if (err) {
      console.error(err)
    }
  })
  res.redirect('/')
}

const postLogin = (req, res) => {
  try {
    res.redirect('/')
  } catch (ex) {
    console.error(ex)
    res.redirect('/')
  }
}

const getJoin = (req, res) => res.render('join', {
  pageTitle: 'join'
})

const postJoin = async (req, res) => {
  try {
    const { body: { email, password } } = req
    const users = await User.create({
      email,
      password
    })
    res.redirect('/')
  } catch (ex) {
    console.error(ex.stack)
    res.redirect('/join')
  }
}

const getProfile = (req, res) => {
  res.render('profile', {
    pageTitle: 'profile'
  })
}

const postProfile = async (req, res) => {
  try {
    const { name, tel, sex, birth } = req.body
    const user = await User.findOneAndUpdate({ _id: req.user._id },
      { $set: { name, tel, sex, birth } })
    console.log(user)
  } catch (err) {
    console.error(err)
  }
  await res.redirect('/profile')
}

const postAvatar = async (req, res) => {
  try {
    await User.findOneAndUpdate({ _id: req.user._id },
      { $set: { avatar: req.file.path } })
  } catch (err) {
    console.error(err)
  }
  await res.redirect('/profile')
}

module.exports = {
  getLogin,
  getLogout,
  postLogin,
  getJoin,
  postJoin,
  getProfile,
  postProfile,
  postAvatar
}
