const Video = require('../models/Video')

let videos = []
const getAllVideos = async () => {
  return await Video.find({})
}

const home = async (req, res) => {
  videos = await getAllVideos()
  return res.render('index', {
    pageTitle: 'home',
    messages: req.flash('success'),
    isAuthenticated: req.user,
    videos: videos
  })
}

module.exports = {
  home
}
