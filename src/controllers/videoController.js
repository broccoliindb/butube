const Video = require('../models/Video')

const getUpload = async (req, res) => {
  let videos = []
  try {
    const { user: { _id } } = req
    videos = await Video.find({
      author: _id
    }).populate('author')
    console.log(videos)

  } catch (err) {
    console.error(err)
  } finally {
    res.render('upload', {
      pageTitle: 'upload',
      videos
    })
  }
}

const postUpload = async (req, res) => {
  // console.log(req.body)
  // console.log(req.file)
  const { videoTitle: title, videoDescription: description, videoTags, genres } = req.body
  const { path: fileUrl } = req.file
  // console.log('aaaaaa')
  const newVideo = await Video.create({
    fileUrl,
    title,
    description,
    genres: genres.split(','),
    author: req.user._id,
    tags: videoTags.split('#')
  })

  res.redirect('/')
  // console.log(2)
}

module.exports = {
  getUpload,
  postUpload
}
