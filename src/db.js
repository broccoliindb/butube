const mongoose = require('mongoose')
require('dotenv').config()
mongoose.connect(
  process.env.MONGO_DB, {
    useNewUrlParser: true,
    useFindAndModify: false,
    useUnifiedTopology: true,
    useCreateIndex: true
  }
)

const db = mongoose.connection

const errorHandler = (err) => console.error(`❌ error occurs ${err.stack}`)
const successHandler = () => console.log('✅ DB connected')

db.on('error', errorHandler)
db.once('open', successHandler)
