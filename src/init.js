require('dotenv').config()
require('./db')

const app = require('./app')

const listeningHandler = () => {
  console.log(`✅ listening on ${process.env.PORT}`)
}

app.listen(process.env.PORT, listeningHandler)
