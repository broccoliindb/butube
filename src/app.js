const express = require('express')
const app = express()
const path = require('path')
const session = require('express-session')
const flash = require('connect-flash')
const FileStore = require('session-file-store')(session)
const { localMiddleware } = require('./middleware')
const userRouter = require('./routes/userRouter')
const videoRouter = require('./routes/videoRouter')
const globalRouter = require('./routes/globalRouter')
const { notFoundError, unCaughtError } = require('./errorHandler')
const passport = require('./passport')
const apis = require('./apis/v1')

app.set('view engine', 'pug')
app.set('views', path.join(__dirname, 'views'))
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(session({
  secret: process.env.SECRET,
  resave: false,
  saveUninitialized: true,
  store: new FileStore()
}))
app.use(flash())
app.use(passport.initialize())
app.use(passport.session())
app.use(localMiddleware)
app.use('/uploads', express.static('uploads'))
app.use('/public', express.static('src/public'))
app.use(apis)
app.use(globalRouter)
app.use(userRouter)
app.use(videoRouter)

app.use(notFoundError)
app.use(unCaughtError)

module.exports = app
