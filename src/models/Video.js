const mongoose = require('mongoose')

const VideoSchema = mongoose.Schema({
  fileUrl: {
    type: String,
    required: true
  },
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    lowercase: true,
    trim: true
  },
  genres: [],
  author: {
    type: mongoose.Types.ObjectId,
    ref: 'User',
    required: true
  },
  tags: [],
  visit: {
    Number,
    default: 0
  },
  comments: {
    type: [
      { type: mongoose.Schema.Types.ObjectId, ref: 'Comment' }
    ]
  }
}, {
  timestamps: true
})

const model = mongoose.model('Video', VideoSchema)

module.exports = model
