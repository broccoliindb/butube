const mongoose = require('mongoose')

const UserSchema = mongoose.Schema({
  email: {
    type: String,
    minlength: 3,
    lowercase: true,
    unique: true,
    trim: true,
    required: true
  },
  password: {
    type: String,
    trim: true,
    required: true
  },
  avatar: {
    type: String
  },
  tel: {
    type: String
  },
  gender: {
    type: String
  },
  birth: {
    type: String
  },
  nickName: String,
  lastName: String,
  firstName: String
}, { timestamps: true })

UserSchema.virtual('birthDisplay').get(function () {
  const date = new Date(this.birth)
  return `${date.getFullYear()}년 ${date.getMonth() + 1}월 ${date.getDate()}일`
})

const model = mongoose.model('User', UserSchema)

module.exports = model
