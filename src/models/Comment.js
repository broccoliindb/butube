const mongoose = require('mongoose')
const CommentSchema = mongoose.Schema({
  content: {
    type: String,
    required: true
  },
  author: {
    type: mongoose.Types.Schema.ObjectId,
    ref: 'User',
    required: true
  }
}, {
  timestamps: true
})

const model = mongoose.model('Comment', CommentSchema)
module.exports = model
