const notFoundError = (req, res, next) => {
  res.send('NOT Found')
  next()
}

const unCaughtError = (err, req, res, next) => {
  if (err) {
    console.error(err.stack)
    res.send(err.stack)
  }
  next(err)
}

module.exports = {
  notFoundError,
  unCaughtError
}
