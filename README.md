# BUTUBE

clone youtube programing with nodejs, mongodb, pug

## TODO

- [ ] 로그인 기능 만들기
- [ ] layout 뼈대만들기
  - [ ] header: 로고 검색창 로그인 로그아웃
  - [ ] body: 홈: 비디오리스트
  - [ ] body: 상세: 비디오, 해쉬태그, 타이틀, 조회수, 날짜, (수정, 삭제) ,코멘트
- [ ] 로그인기능
- [ ] 업로드기능  
- [ ] node version fix하기
- [ ] api swagger 만들기
- [ ] refactoring
 

## Routes

- get
  - / : home
  - /login: 로그인
  - /:id : 비디오상세
- post
  - /login
  - /:id/upload
  - /:id/delete
  - /:id/update

 